import CollectionConfigInterface from '../lib/CollectionConfigInterface';
import whitelistAddresses from './whitelist.json';

const CollectionConfig: CollectionConfigInterface = {
  // The contract name can be updated using the following command:
  // yarn rename-contract NEW_CONTRACT_NAME
  // Please DO NOT change it manually!
  contractName: 'YourNftToken',
  tokenName: 'Ownable Asset',
  tokenSymbol: 'OWNA',
  hiddenMetadataUri: 'ipfs://QmNkFb6DpVR92chP3y4CvPVanoU57Hx97jS6bMCWuqXNP1',
  maxSupply: 100,
  whitelistSale: {
    price: 0.01,
    maxMintAmountPerTx: 1,
  },
  preSale: {
    price: 0.03,
    maxMintAmountPerTx: 2,
  },
  publicSale: {
    price: 0.05,
    maxMintAmountPerTx: 5,
  },
  contractAddress: "0xfA9C4b00Fba9cf6FfaC1601a535D707879d43A58",
  openSeaSlug: 'ownable-asset',
  whitelistAddresses: whitelistAddresses,
};

export default CollectionConfig;
