// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

/// [MIT License]
/// @title Base64
/// @notice Provides a function for encoding some bytes in base64
/// @author Brecht Devos <brecht@loopring.org>
library Base64 {
    bytes internal constant TABLE =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    /// @notice Encodes some bytes to the base64 representation
    function encode(bytes memory data) internal pure returns (string memory) {
        uint256 len = data.length;
        if (len == 0) return '';

        // multiply by 4/3 rounded up
        uint256 encodedLen = 4 * ((len + 2) / 3);

        // Add some extra buffer at the end
        bytes memory result = new bytes(encodedLen + 32);

        bytes memory table = TABLE;

        assembly {
            let tablePtr := add(table, 1)
            let resultPtr := add(result, 32)

            for {
                let i := 0
            } lt(i, len) {

            } {
                i := add(i, 3)
                let input := and(mload(add(data, i)), 0xffffff)

                let out := mload(add(tablePtr, and(shr(18, input), 0x3F)))
                out := shl(8, out)
                out := add(
                    out,
                    and(mload(add(tablePtr, and(shr(12, input), 0x3F))), 0xFF)
                )
                out := shl(8, out)
                out := add(
                    out,
                    and(mload(add(tablePtr, and(shr(6, input), 0x3F))), 0xFF)
                )
                out := shl(8, out)
                out := add(
                    out,
                    and(mload(add(tablePtr, and(input, 0x3F))), 0xFF)
                )
                out := shl(224, out)

                mstore(resultPtr, out)

                resultPtr := add(resultPtr, 4)
            }

            switch mod(len, 3)
            case 1 {
                mstore(sub(resultPtr, 2), shl(240, 0x3d3d))
            }
            case 2 {
                mstore(sub(resultPtr, 1), shl(248, 0x3d))
            }

            mstore(result, encodedLen)
        }

        return string(result);
    }
}

/// @author Modified version of original code by 1001.digital
/// @title A token tracker that limits the token supply and increments token IDs on each new mint.
abstract contract WithLimitedSupply {
	// Keeps track of how many we have minted
	uint256 private _tokenCount;

	/// @dev The maximum count of tokens this token tracker will issue.
	uint256 private immutable _maxAvailableSupply;

	/// Instanciate the contract
	/// @param maxSupply_ how many tokens this collection should hold
	constructor(uint256 maxSupply_, uint256 reserved_) {
		_maxAvailableSupply = maxSupply_ - reserved_;
	}

	function maxAvailableSupply() public view returns (uint256) {
		return _maxAvailableSupply;
	}

	/// @dev Get the current token count
	/// @return the created token count
	/// TODO: if this is not required externally, does making it `public view` use unneccary gas?
	function tokenCount() public view returns (uint256) {
		return _tokenCount;
	}

	/// @dev Check whether tokens are still available
	/// @return the available token count
	function availableTokenCount() public view returns (uint256) {
		return maxAvailableSupply() - tokenCount();
	}

	/// @dev Increment the token count and fetch the latest count
	/// @return the next token id
	function nextToken() internal virtual ensureAvailability returns (uint256) {
		return _tokenCount++;
	}

	/// @dev Check whether another token is still available
	modifier ensureAvailability() {
		require(availableTokenCount() > 0, 'No more tokens available');
		_;
	}

	/// @param amount Check whether number of tokens are still available
	/// @dev Check whether tokens are still available
	modifier mintCompliance(uint256 amount) {
		require(
			availableTokenCount() >= amount,
			'Requested number of tokens not available'
		);
		_;
	}
}

/// @author Modified version of original code by 1001.digital
/// @title Randomly assign tokenIDs from a given set of tokens.
abstract contract RandomlyAssigned is WithLimitedSupply {
	// Used for random index assignment
	mapping(uint256 => uint256) private tokenMatrix;

	// The initial token ID
	uint256 private immutable startFrom;

	/// Instanciate the contract
	/// @param maxSupply_ how many tokens this collection should hold
	/// @param numReserved_ the number of tokens reserved whose IDs dont come from the randomizer
	constructor(uint256 maxSupply_, uint256 numReserved_)
		WithLimitedSupply(maxSupply_, numReserved_)
	{
		startFrom = numReserved_ + 1;
	}

	/// Get the next token ID
	/// @dev Randomly gets a new token ID and keeps track of the ones that are still available.
	/// @return the next token ID
	function nextToken() internal override returns (uint256) {
		uint256 maxIndex = maxAvailableSupply() - tokenCount();
		uint256 random = uint256(
			keccak256(
				abi.encodePacked(
					msg.sender,
					block.coinbase,
					block.difficulty,
					block.gaslimit,
					block.timestamp
				)
			)
		) % maxIndex;

		uint256 value = 0;
		if (tokenMatrix[random] == 0) {
			// If this matrix position is empty, set the value to the generated random number.
			value = random;
		} else {
			// Otherwise, use the previously stored number from the matrix.
			value = tokenMatrix[random];
		}

		// If the last available tokenID is still unused...
		if (tokenMatrix[maxIndex - 1] == 0) {
			// ...store that ID in the current matrix position.
			tokenMatrix[random] = maxIndex - 1;
		} else {
			// ...otherwise copy over the stored number to the current matrix position.
			tokenMatrix[random] = tokenMatrix[maxIndex - 1];
		}

		// Increment counts (ie. qty minted)
		super.nextToken();

		return value + startFrom;
	}
}

abstract contract ContextMixin {
    function msgSender()
        internal
        view
        returns (address payable sender)
    {
        if (msg.sender == address(this)) {
            bytes memory array = msg.data;
            uint256 index = msg.data.length;
            assembly {
                // Load the 32 bytes word from memory with the address on the lower 20 bytes, and mask those.
                sender := and(
                    mload(add(array, index)),
                    0xffffffffffffffffffffffffffffffffffffffff
                )
            }
        } else {
            sender = payable(msg.sender);
        }
        return sender;
    }
}

/**
 * @title ERC721Tradable
 * ERC721Tradable - ERC721 contract that whitelists a trading address, and has minting functionality.
 */
abstract contract ERC721Tradable is ERC721, ContextMixin, Ownable, RandomlyAssigned, ReentrancyGuard {
    using SafeMath for uint256;
    using Counters for Counters.Counter;

    Counters.Counter private _nextTokenId;

	bytes32 public merkleRoot;
  	mapping(address => bool) public whitelistClaimed;


	/*
	 * Private Variables
	 */
	uint256 private constant NUMBER_OF_GENESIS_TOKENS = 0; 
	uint256 private constant NUMBER_OF_RESERVED_TOKENS = 0;
	uint256 private MAX_SUPPLY; 

	/*
	 * Public Variables
	 */
	uint256 public maxSupply;
	uint256 public cost;
	uint256 public maxMintAmountPerTx;

	bool public paused = true;
	bool public whitelistMintEnabled = false;
	bool public revealed = false;
	
	/*
	 * Private Variables
	 */
	string private uriPrefix = ""; // IPFS URI WILL BE SET AFTER ALL TOKENS SOLD OUT
	string private hiddenMetadataUri = ""; // ipfs://QmNkFb6DpVR92chP3y4CvPVanoU57Hx97jS6bMCWuqXNP1
	string private uriSuffix = ".json";

    constructor(
        string memory _name,
        string memory _symbol,
		uint256 _cost,
		uint256 _maxSupply,
		uint256 _maxMintAmountPerTx,
		string memory _hiddenMetadataUri
    ) 
    ERC721(_name, _symbol) 
    RandomlyAssigned(
			_maxSupply,
			NUMBER_OF_GENESIS_TOKENS + NUMBER_OF_RESERVED_TOKENS
		)
    {
		cost = _cost;
		MAX_SUPPLY = _maxSupply;
		maxSupply = _maxSupply;
		maxMintAmountPerTx = _maxMintAmountPerTx;
		setHiddenMetadataUri(_hiddenMetadataUri);
	}

	function setUriPrefix(string memory _uriPrefix) public onlyOwner {
    	uriPrefix = _uriPrefix;
	}

	function setUriSuffix(string memory _uriSuffix) public onlyOwner {
		uriSuffix = _uriSuffix;
	}

	function setPaused(bool _state) public onlyOwner {
		paused = _state;
	}

	function setMerkleRoot(bytes32 _merkleRoot) public onlyOwner {
		merkleRoot = _merkleRoot;
	}

	function setWhitelistMintEnabled(bool _state) public onlyOwner {
		whitelistMintEnabled = _state;
	}

	modifier mintPriceCompliance(uint256 _mintAmount) {
		require(msg.value >= cost * _mintAmount, "Insufficient funds!");
    	_;
	}
	
	function setRevealed(bool _state) public onlyOwner {
		revealed = _state;
	}

	function setCost(uint256 _cost) public onlyOwner {
		cost = _cost;
	}

	function setMaxMintAmountPerTx(uint256 _maxMintAmountPerTx) public onlyOwner {
		maxMintAmountPerTx = _maxMintAmountPerTx;
	}

	function setHiddenMetadataUri(string memory _hiddenMetadataUri) public onlyOwner {
		hiddenMetadataUri = _hiddenMetadataUri;
	}

	/***
     * @dev internal check to ensure a genesis token ID, or ID outside of the collection, doesn't get minted
	 *
	 */
	function _mintRandomId(address to) private returns (uint256) {
		uint256 id = nextToken();
		assert(
			id > NUMBER_OF_GENESIS_TOKENS + NUMBER_OF_RESERVED_TOKENS &&
				id <= MAX_SUPPLY
		);
		_safeMint(to, id);
		return id;
	}

    /**
     * @dev Mints a token to an address with a tokenURI.
     * @param _to address of the future owner of the token
     */
    function mintTo(address _to) public onlyOwner {
		_mintRandomId(_to);
    }

	function mint(uint256 _mintAmount) public payable mintCompliance(_mintAmount) mintPriceCompliance(_mintAmount) {
		require(_mintAmount > 0 && _mintAmount <= maxMintAmountPerTx, "Invalid mint amount!");
		require(!paused, "The contract is paused!");

		_mintLoop(msg.sender, _mintAmount);
	}

	function whitelistMint(uint256 _mintAmount, bytes32[] calldata _merkleProof) external payable mintCompliance(_mintAmount) mintPriceCompliance(_mintAmount) {
		require(_mintAmount > 0 && _mintAmount <= maxMintAmountPerTx, "Invalid mint amount!");
		require(whitelistMintEnabled, "The whitelist sale is not enabled!");
		require(!whitelistClaimed[msg.sender], "Address already claimed!");
		bytes32 leaf = keccak256(abi.encodePacked(msg.sender));
		require(MerkleProof.verify(_merkleProof, merkleRoot, leaf), "Invalid proof!");

		whitelistClaimed[msg.sender] = true;

		_mintLoop(msg.sender, _mintAmount);
	}

	function _mintLoop(address _receiver, uint256 _mintAmount) internal {
		for (uint256 i; i < _mintAmount; i++) {
			_mintRandomId(_receiver);
		}
	}

    /**
        @dev Returns the max total tokens for minted.
     */
    function totalSupply() public view returns (uint256) {
        return tokenCount();
    }

	function withdraw(
        address _to
    ) public payable onlyOwner nonReentrant {
		(bool os, ) = payable(_to).call{ value: address(this).balance }("");
        require(os);
    }

	function mintForAddress(uint256 _mintAmount, address _receiver) public mintCompliance(_mintAmount) onlyOwner {
		_mintLoop(_receiver, _mintAmount);
	}

	function walletOfOwner(address _owner)
		public
		view
		returns (uint256[] memory)
	{
		uint256 ownerTokenCount = balanceOf(_owner);
		uint256[] memory ownedTokenIds = new uint256[](ownerTokenCount);
		uint256 currentTokenId = 1;
		uint256 ownedTokenIndex = 0;

		while (ownedTokenIndex < ownerTokenCount && currentTokenId <= MAX_SUPPLY) {
		address currentTokenOwner = ownerOf(currentTokenId);

		if (currentTokenOwner == _owner) {
			ownedTokenIds[ownedTokenIndex] = currentTokenId;

			ownedTokenIndex++;
		}

		currentTokenId++;
		}

		return ownedTokenIds;
	}


	function hiddenTokenURI(uint256 _tokenId) private view returns (string memory) {
        return string(
            abi.encodePacked(
                "data:application/json;base64,",
                Base64.encode(
                    bytes(
                        abi.encodePacked(
                            '{"name":"'
                            'Not revealed yet (#', Strings.toString(_tokenId),')", "description": "All NFTs were not revealed yet."',
                            ', "image":"', hiddenMetadataUri, 
							'", "attributes": []}'
                        )
                    )
                )
            )
        );
    }

	/**
	 * Set the base URI for the metadata
	 * @dev modifies the state of the `uriPrefix` variable
	 * @param URI the URI to set as the base token URI
	 */
	function _baseURI(string memory URI) external onlyOwner {
		uriPrefix = URI;
	}

	function setHiddenURI(string memory URI) external onlyOwner {
		hiddenMetadataUri = URI;
	}

	/**
	* Return the tokenURI for a given ID
	* @dev overrides ERC721's `tokenURI` function and returns either the `uriPrefix` or a custom URI
	* @notice reutrns the tokenURI using the `_tokenBase` URI if the token ID hasn't been suppleid with a unique custom URI
	*/
	function tokenURI(uint256 tokenId)
		public
		view
		override(ERC721)
		returns (string memory)
	{
		require(_exists(tokenId), 'Cannot query non-existent token');

		if (!revealed) {
			return hiddenTokenURI(tokenId);
		}

		return
			bytes(uriPrefix).length > 0
				? string(abi.encodePacked(uriPrefix, Strings.toString(tokenId), uriSuffix))
				: '';
	}

    /**
     * Override isApprovedForAll to whitelist user's OpenSea proxy accounts to enable gas-less listings.
     */
    function isApprovedForAll(address owner, address operator)
        override
        public
        view
        returns (bool)
    {
        return super.isApprovedForAll(owner, operator);
    }

    /**
     * This is used instead of msg.sender as transactions won't be sent by the original token owner, but by OpenSea.
     */
    function _msgSender()
        internal
        override
        view
        returns (address sender)
    {
        return ContextMixin.msgSender();
    }
}

/**
 * @title Creature
 * Creature - a contract for my non-fungible creatures.
 */
contract YourNftToken is ERC721Tradable {
    constructor(
		string memory _tokenName,
		string memory _tokenSymbol,
		uint256 _cost,
		uint256 _maxSupply,
		uint256 _maxMintAmountPerTx,
		string memory _hiddenMetadataUri
	)
        ERC721Tradable(
			_tokenName,
			_tokenSymbol,
			_cost,
			_maxSupply,
			_maxMintAmountPerTx,
			_hiddenMetadataUri
		)
    {}
}